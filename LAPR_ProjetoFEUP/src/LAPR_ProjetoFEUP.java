import java.io.*;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Scanner;

import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;


//-----
public class LAPR_ProjetoFEUP {
    static Scanner read = new Scanner(System.in);
    public static boolean modoInterativo;
    public static String fileType;                                 //formato do ficheiro
    public static boolean excecao;
    public static double[] auxiliarTmv = new double[2];
    public static String especie;
    public static String filename;
    public static String fileName;                                 //modo nao interativo
    public static int numberOfGenerations;
    public static int numberOfGenerationsFicheiro;
    public static double[][] populationVectorFicheiro;
    public static double[] survivalMatrixFicheiro;
    public static double[] birthMatrixFicheiro;
    public static double[][] matrizLeslieNaoInterativo;
    public static double[][] populationVector;
    public static int ages;
    public static double[] birthMatrix = new double[ages];
    public static double[] survivalArray = new double[ages];
    public static double[][] matrizLeslie = new double[survivalArray.length][birthMatrix.length];
    public static double[][] matrizLeslieNI;
    /////////////////JOAO/////////////////////////////
    public static char c = ' ';
    public static Scanner sc = new Scanner(System.in);
    public static String[] nomeFicheiros = new String[]{"total.txt", "taxadevariacao.txt", "distribuicaonaonormalizada.txt", "distribuicaooNormalizada.txt"};
    public static String[] titulos = new String[]{"populacao total", "taxa de variacao", "distribuicao da populacao", "distribuicao da populacao normalizada"};
    public static String[] ylabel = new String[]{"número de indivíduos", "variação da população total", "número de indivíduos", "(%) da população total"};
    public static String[] tituloSecundario = new String[]{"", "", "classe ", "classe "};
    public static String[] nomeFicheiroGnu = new String[]{"plotTotal.gp", "plotTaxaDeVariação.gp", "plotDistribuiçãoNãoNormalizada.gp", "plotDistribuiçãoNormalizada.gp"};
    ///////////////////////JOAO//////////////////////////////////

    public static void main(String[] args) throws FileNotFoundException {
        //Programa começa por ler os argumentos digitados na cmd
        if (args.length == 0) {     // java -jar nome_programa    interativo opção de introduzir valores
            modoInterativo=true;
            System.out.println("Indique o nome da espécie: ");
            especie = read.nextLine();                            //nome da especie
            System.out.println("Introdução dos valores iniciais");     //o programa obriga o utilizador a inserir os valores iniciais de população e das taxas
            ages = numberOfAgeRanges();
            matrizLeslie = fillMatrix(survivalArray(ages), birthArray(ages));
            printMatrix(matrizLeslie);
            fillVector(ages);
            System.out.println("Introduza o numero de gerações: ");
            numberOfGenerations = read.nextInt();
            //----
            menuLoop();
            //colocar os metodos

        } else {
            if (args.length == 2) {  // java -jar nome_programa -n nome_ficheiro
                modoInterativo = true;
                filename = args[1];
                leFicheiro(filename);
                matrizLeslieNaoInterativo = readFileInfo(filename);  //Ler os valores do ficheiro para a matriz de Leslie e vetor de população inicial
                printMatrix(matrizLeslieNaoInterativo);
                System.out.println("Digite o numero de gerações:");
                numberOfGenerationsFicheiro = read.nextInt();
                menuLoopFicheiro();
                //colocar os metodos
            } else {
                if ((args.length > 2) && (args.length < 10)) {                  //o modo nao interativo pede no máximo 9 argumentos ao utilizador (e, v, r não são obrigatórios)
                    modoInterativo = false;
                    int formatoGnuplot;
                    //modo interativo = false                                   java -jar nome_programa -t XXX -g Y -e -v -r ficheiro_entrada.txt ficheiro_saida.txt
                    fileName = args[args.length - 2];                    // nome do ficheiro no modo nao interativo
                    leFicheiro(fileName);
                    matrizLeslieNI = readFileInfo(fileName);
                    String outPutFileName = args[args.length - 1];               //nome do ficheiro de output no modo nao interativo
                    for (int i = 0; i < args.length; i++) {
                        if (args[i].equals("-t")) {
                            numberOfGenerations = Integer.parseInt(args[i + 1]);
                        }
                        if (args[i].equals("-g")) {
                            formatoGnuplot = Integer.parseInt(args[i + 1]);
                        }
                    }
                    runNaoInterativo(args, matrizLeslieNI, populationVectorFicheiro, numberOfGenerations);

                   // modoNaoInterativo(matrizLeslieNI, populationVectorFicheiro, numberOfGenerations); //recebe todas as variáveis (falta variaveis)
                } else {
                    System.out.println("Argumentos inválidos.");
                }
            }
        }
    }

    public static void leFicheiro(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        String s = sc.nextLine();
        char c;
        int cont = 1;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if (c == ',') {
                cont++;
            }
        }
        matrizLeslieNaoInterativo = new double[cont][cont];
        survivalMatrixFicheiro = new double[cont];
        birthMatrixFicheiro = new double[cont];
        populationVectorFicheiro = new double[cont][1];
    }

    public static void menuModoInterativo() {
        System.out.println("MODO INTERATIVO");
        System.out.println("1-Gerar distribuição normal e normalizada da população.");
        System.out.println("2-Calcular dimensão da população e taxa de variação.");
        System.out.println("3-Valores e vetores próprios(comportamento assintótico)");
        System.out.println("4-Gerar representação gráfica");
        System.out.println("0-SAIR");
    }

    public static int lerOpcao() throws FileNotFoundException {
        int opcao = read.nextInt();
        switch (opcao) {
            case 1:
                //metodos para calcular distribuição a partir dos dados introduzidos
                return 1;
            case 2:
                dimensaoDaPopulacao(matrizLeslie, populationVector, numberOfGenerations); // CALCULA DIMENSÃO DA POPULAÇÃO - Guilherme
                registaTaxaVariacao(matrizLeslie, populationVector, numberOfGenerations); // VALOR DA TAXA DE VARIAÇÃO - Guilherme
                return 2;
            case 3:
                analiseassintotica(matrizLeslie);
                return 3;
            case 4:
                organizaDados(matrizLeslie, populationVector, especie, numberOfGenerations);
                return 4;
            default:
                return 0;
        }
    }

    public static int lerOpcaoFicheiro() throws FileNotFoundException {
        int opcao = read.nextInt();
        switch (opcao) {
            case 1:
                //calcular distribuição a partir dos ficheiros
                return 1;
            case 2:
                dimensaoDaPopulacao(matrizLeslieNaoInterativo, populationVectorFicheiro, numberOfGenerationsFicheiro); // CALCULA DIMENSÃO DA POPULAÇÃO - Guilherme
                registaTaxaVariacao(matrizLeslieNaoInterativo, populationVectorFicheiro, numberOfGenerationsFicheiro); // VALOR DA TAXA DE VARIAÇÃO - Guilherme
                return 2;
            case 3:
                analiseassintotica(matrizLeslieNaoInterativo);
                return 3;
            case 4:
                organizaDados(matrizLeslieNaoInterativo, populationVectorFicheiro, filename, numberOfGenerationsFicheiro);
                return 4;
            default:
                return 0;
        }
    }

    public static void menuLoopFicheiro() throws FileNotFoundException {
        menuModoInterativo();
        int a = lerOpcaoFicheiro();
        while (a != 0) {
            menuModoInterativo();
            a = lerOpcaoFicheiro();
        }
    }

    public static void menuLoop() throws FileNotFoundException {
        menuModoInterativo();
        int a = lerOpcao();
        while (a != 0) {
            menuModoInterativo();
            a = lerOpcao();
        }
    }

    //================================================================================================================


    public static void modoNaoInterativo(double[][] matriz, double[][] total, double[][] taxaDeVariacao, double[][] distribuicaoPopulacao, double[][] distribuicaoNormalizada, double[] vetorProprio, double valorProprio, int numeroGeracoes) throws FileNotFoundException {
        PrintWriter pt = new PrintWriter(new File("modoNaoInterativo.txt"));
        pt.println("K=" + numeroGeracoes + "\n");
        pt.println("Matriz de Leslie");
        int i, i2;
        for (i = 0; i < matriz.length; i++) {
            for (i2 = 0; i2 < matriz[i].length - 1; i2++) {
                pt.print(matriz[i][i2] + ", ");
            }
            pt.print(matriz[i][i2]);
            pt.println();
        }
        pt.println("Número total de indivíduos");
        pt.println("(t, Nt)");
        for (i = 0; i < total.length; i++) {
            pt.println("(" + i + ", " + total[i] + ")");
        }
        pt.println("\n");
        pt.println("Crescimento da população");
        pt.println("(t, delta_t)");
        for (i = 0; i < taxaDeVariacao.length; i++) {
            pt.println("(" + i + ", " + taxaDeVariacao[i] + ")");
        }
        pt.println();
        pt.println("Número por classe (não normalizado)");
        pt.print("(t,");
        for (i = 0; i < numeroGeracoes; i++) {
            pt.println(" x" + i + 1 + ",");
        }
        pt.println(")");
        //distribuição
        pt.println("Número por classe (normalizado)");
        pt.print("(t,");
        for (i = 0; i < numeroGeracoes; i++) {
            pt.println(" x" + i + 1 + ",");
        }
        pt.println(")");
        for (i = 0; i < distribuicaoNormalizada.length; i++) {
            pt.print("(" + i);
            for (i2 = 0; i2 < distribuicaoNormalizada[i].length - 1; i2++) {
                pt.print(" " + distribuicaoNormalizada[i][i2] + ",");
            }
            pt.println(" " + distribuicaoNormalizada[i][i2] + ")");
        }
        pt.println();
        pt.println("Maior valor próprio e vetor associado");
        pt.println("lambda=" + valorProprio);
        pt.print("vetor proprio associado=(");
        for (i = 0; i < vetorProprio.length - 1; i++) {
            pt.print(vetorProprio[i] + ",");
        }
        pt.println(vetorProprio[i] + ")");
        pt.close();
    }

    public static void runNaoInterativo(String[] args, double[][] matrizLeslieNI, double[][] populationVector, int numberOfGenerations) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-e")) {
                analiseassintotica(matrizLeslieNI); //calcular vetores
            }
            if (args[i].equals("-v")) {
                dimensaoDaPopulacao(matrizLeslieNI, populationVector, numberOfGenerations);
                registaTaxaVariacao(matrizLeslieNI, populationVector, numberOfGenerations);
            }
            if (args[i].equals("-r")) {
                //variação da população
            }
        }
    }


    //---------------------------------------Interativo Opção 1 --------------------------------------------------------
    public static int numberOfAgeRanges() {        // Interativo Opção 1
        int numberOfAgeRanges;
        System.out.println("Indique o número de faixas etárias que pretende: ");
        numberOfAgeRanges = read.nextInt();
        while (numberOfAgeRanges <= 0) {
            System.out.println("Por favor insira um número maior do que 0 (zero)");
            numberOfAgeRanges = read.nextInt();
        }
        return numberOfAgeRanges;
    }


    public static double[][] fillVector(int ages) {            //Preencher vetor (coluna) de distribuição inicial
        populationVector = new double[ages][1];
        for (int i = 0; i < ages; i++) {
            for (int j = 0; j < 1; j++) {
                System.out.println("Introduzir valor: ");
                populationVector[i][0] = read.nextInt();
            }
        }
        return populationVector;
    }


    public static double[] survivalArray(int ages) {        // Preencher Array da taxa de sobrevivência
        survivalArray = new double[ages];
        for (int i = 0; i < ages; i++) {
            System.out.println("Introduzir valor da taxa de sobrevivência: ");
            survivalArray[i] = read.nextDouble();
        }
        return survivalArray;
    }


    public static double[] birthArray(int ages) {           //Preencher Array da taxa de natalidade
        birthMatrix = new double[ages];
        for (int i = 0; i < ages; i++) {
            System.out.println("Introduzir valor da taxa de natalidade: ");
            birthMatrix[i] = read.nextDouble();
        }
        return birthMatrix;
    }


    public static double[][] fillMatrix(double[] survivalMatrix, double[] birthMatrix) {

        double[][] matrizLeslie = new double[survivalMatrix.length][birthMatrix.length];
        for (int i = 0; i < birthMatrix.length; i++) {
            matrizLeslie[0][i] = birthMatrix[i];         // preenche a primeira linha
        }
        int count = 0;
        for (int i = 1; i < survivalMatrix.length; i++) {
            for (int j = 0; j < survivalMatrix.length; j++) {
                if (i == j + 1) {
                    matrizLeslie[i][j] = survivalMatrix[count];
                    count++;
                } else {
                    matrizLeslie[i][j] = 0;
                }
            }
        }
        return matrizLeslie;
    }


    //--------------------------------------- Modo de Ficheiro------------------------------------------------------- //


    public static double[][] readFileInfo(String fileName) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new File(fileName));
        String[] primeiraLinha = fileReader.nextLine().trim().split(",");
        populationVectorFicheiro = getTotalPopulacao(primeiraLinha);

        String[] segundaLinha = fileReader.nextLine().trim().split(",");
        double[] survivalMatrix = getValores(segundaLinha);

        String[] terceiraLinha = fileReader.nextLine().trim().split(",");
        double[] birthMatrix = getValores(terceiraLinha);

        double[][] matrizLeslieNaoInterativo = fillMatrixFicheiro(survivalMatrix, birthMatrix);
        return matrizLeslieNaoInterativo;
    }


    public static double[] getValores(String[] linha) {
        double[] matriz = new double[linha.length];
        for (int i = 0; i < linha.length; i++) {
            String[] objeto = linha[i].split("=");
            matriz[i] = Double.parseDouble(objeto[1]);
        }
        return matriz;
    }

    public static double[][] getTotalPopulacao(String[] linha) {
        double[][] matriz = new double[linha.length][1];

        for (int i = 0; i < linha.length; i++) {
            String[] objeto = linha[i].split("=");
            matriz[i][0] = Integer.parseInt(objeto[1]);
        }

        return matriz;
    }

    public static void printMatrix(double[][] matriz) {
        for (int i = 0; i < matriz.length; i++)//Cycles through rows
        {
            for (int j = 0; j < matriz[i].length; j++)//Cycles through columns
            {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println(); //Makes a new row
        }
    }

    public static double[][] fillMatrixFicheiro(double[] survivalMatrix, double[] birthMatrix) {
        for (int i = 0; i < birthMatrix.length; i++) {
            matrizLeslieNaoInterativo[0][i] = birthMatrix[i];         // preenche a primeira linha
        }
        int count = 0;
        for (int i = 1; i < matrizLeslieNaoInterativo.length; i++) {
            for (int j = 0; j < matrizLeslieNaoInterativo[i].length; j++) {
                if (i == j + 1) {
                    matrizLeslieNaoInterativo[i][j] = survivalMatrix[count];
                    count++;
                } else {
                    matrizLeslieNaoInterativo[i][j] = 0;
                }
            }
        }
        return matrizLeslieNaoInterativo;
    }


    //------GUILHERME CUNHA----- START

//===============================================MÉTODOS PRINCIPAIS=====================================================

    // DIMENSAO DA POPULACAO EM t //

    public static void dimensaoDaPopulacao(double[][] matrizLeslie, double[][] populationVector, int numberOfGenerations) {
        DecimalFormat df = new DecimalFormat("####.##");
        System.out.println("Dimensão da população em t:");

        int cont = 0;
        double total = 0;
        int coluna;
        double[][] matriz;
        matriz = new double[matrizLeslie.length][matrizLeslie.length];
        double[][] matrizSoma;
        matrizSoma = new double[matrizLeslie.length][matrizLeslie.length];

        if (numberOfGenerations > 1) {
            coluna = numberOfGenerations - 1;
        } else {
            coluna = numberOfGenerations;
        }

        if (numberOfGenerations > 1) { //geração != 1 && 0
            for (int i = 0; i < matrizLeslie.length; i++) {
                for (int j = 0; j < matrizLeslie.length; j++) {

                    matriz[i][j] = matrizLeslie[i][j];
                }
            }

            while (cont < coluna) {
                for (int i = 0; i < matrizLeslie.length; i++) {
                    for (int j = 0; j < matrizLeslie.length; j++) {

                        matrizSoma[i][j] = 0;
                    }
                }

                for (int i = 0; i < matrizLeslie.length; i++) {
                    for (int j = 0; j < matrizLeslie.length; j++) {
                        for (int k = 0; k < matrizLeslie.length; k++) {

                            matrizSoma[i][j] += (matriz[i][k] * matrizLeslie[k][j]);
                        }
                    }
                }

                for (int i = 0; i < matrizLeslie.length; i++) {
                    for (int j = 0; j < matrizLeslie.length; j++) {

                        matriz[i][j] = matrizSoma[i][j];

                    }
                }
                cont++;
            }
            //somatório
            for (int i = 0; i < matrizLeslie.length; i++) {
                for (int j = 0; j < matrizLeslie.length; j++) {

                    total += ((matrizSoma[i][j]) * populationVector[j][0]);
                }
            }

        } else { //geração = 0
            if (numberOfGenerations == 0) {
                for (int i = 0; i < populationVector.length; i++) {
                    for (int j = 0; j < populationVector[0].length; j++) {
                        total += populationVector[i][j];
                    }
                }
            } else { //geração=1
                for (int i = 0; i < matrizLeslie.length; i++) {
                    for (int j = 0; j < matrizLeslie.length; j++) {

                        total += ((matrizLeslie[i][j]) * populationVector[j][0]);
                    }
                }
            }
        }

        System.out.println(df.format(auxiliarArredondamento(total, 0.01)));
        System.out.println();
    }

    // TAXA DE VARIAÇÃO //
    public static void registaTaxaVariacao(double[][] matrizLeslie, double[][] populationVector, int numberOfGenerations) {
        double n1 = 0;
        double taxa;
        double[][] matriz1;
        double[][] matrizLeslieExp;

        System.out.println("Taxa de variação (∆t):");
        for (int i = 0; i <= numberOfGenerations + 1; i++) {

            double n = 0;

            if (i == 0) {
                matriz1 = populationVector;

            } else { //i>0

                matrizLeslieExp = exponencialMatrix(matrizLeslie, i);
                matriz1 = calculaProduto(matrizLeslieExp, populationVector);
            }

            for (int j = 0; j < matriz1.length; j++) {
                for (int k = 0; k < matriz1[0].length; k++) {

                    n += matriz1[j][k];

                }
            }

            if (i != 0) {
                taxa = n / n1; //∆t
                System.out.print("[ ");
                System.out.printf("%.2f%% ", taxa);
                System.out.print("]");
                System.out.println();
            }
            n1 = n;
        }

    }
    //=========================================IMPRIMIR NO FICHEIRO===================================================//


    //===============================================AUXILIARES=======================================================//

    public static double[][] calculaProduto(double[][] matriz1, double[][] matriz2) {

        double[][] produto = new double[matriz1.length][matriz2[0].length];

        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; j < matriz2[0].length; j++) {
                for (int k = 0; k < matriz1[0].length; k++) {
                    produto[i][j] += (matriz1[i][k] * matriz2[k][j]);
                }
            }
        }
        return produto;
    }

    public static double[][] exponencialMatrix(double[][] matriz, int numberOfGenerations) {

        double[][] matriz1 = matriz;

        if (numberOfGenerations != 1) {
            for (int i = 1; i < numberOfGenerations; i++) {

                matriz1 = calculaProduto(matriz1, matriz);
            }
        }
        return matriz1;
    }

    public static double auxiliarArredondamento(double valorInicial, double valorArredondamento) {//FUNCIONA//

        double valorfinal = (int) valorInicial;
        do {
            valorfinal += valorArredondamento;
        } while (valorfinal <= valorInicial);
        if (valorInicial < valorfinal - (valorArredondamento / 2)) {
            valorfinal -= valorArredondamento;
        }
        return valorfinal;
    }


    //GUILHERME CUNHA---END

    public static void analiseassintotica(double[][] matriz) {
        Matrix leslie = new Basic2DMatrix(matriz);
        EigenDecompositor eigenD = new EigenDecompositor(leslie);
        Matrix[] mattD = eigenD.decompose();
        double matA[][] = mattD[0].toDenseMatrix().toArray();// matriz com os vetores proprios
        double matB[][] = mattD[1].toDenseMatrix().toArray();// matriz com os valores proprios
        double maiorvalorproprio = matB[0][0];// determina o maior valor proprio
        double vetorassociado[] = new double[matriz.length];
        double sum = 0;
        int col;
        for (col = 0; col < matA.length; col++) {
            sum = sum + matA[col][0];
        }
        for (col = 0; col < vetorassociado.length; col++) {
            vetorassociado[col] = (matA[col][0] / sum) * 100;
        }
        System.out.println("Maior Valor proprio = " + maiorvalorproprio);
        System.out.print("Vetor associado = { ");
        for (col = 0; col < vetorassociado.length; col++) {
            System.out.printf(" %.0f ", vetorassociado[col]);
        }
        System.out.print("}");
        System.out.println();
    }

    ////////////////////////////////////////////////////JOAO///////////////////////////////////////////////
    public static void organizaDados(double[][] matriz, double[][] intervalos, String especie, int n) throws FileNotFoundException {
        double[][] resultados = new double[n + 1][matriz.length];
        double[] total = new double[n + 1];
        double[][] distribuicaoNormalizada = new double[n + 1][matriz.length];
        registaResultados(resultados, matriz, intervalos);
        registaPopulacaoTotal(total, resultados);
        double[] tmv;
        auxiliarTmv[0] = 0;
        auxiliarTmv[1] = 0;
        if (n > 0) {
            tmv = new double[n];
            excecao = false;
        } else {
            excecao = true;
            tmv = new double[2];
            if (n == 0) {
                auxiliarTmv[0] = total[0];
                auxiliarTmv[1] = 0;
            } else {
                auxiliarTmv[1] = total[1] - total[0];
                auxiliarTmv[0] = total[0];
            }
        }
        registaTaxaVariacao(tmv, total);
        registaDistribuicaoNormalizada(total, resultados, distribuicaoNormalizada);
        criaFicheiros(total, tmv, resultados, distribuicaoNormalizada, especie);
    }

    public static void criaFicheiros(double[] total, double[] tmv, double[][] resultados, double[][] distribuicaoNormalizada, String especie) throws FileNotFoundException {
        PrintWriter pt;
        pt = new PrintWriter(nomeFicheiros[0]);
        geraFicheiro(total, pt);
        pt.close();
        if (!excecao) {
            pt = new PrintWriter(nomeFicheiros[1]);
            geraFicheiro(tmv, pt);
            pt.close();
        } else {
            pt = new PrintWriter(nomeFicheiros[1]);
            geraFicheiro(auxiliarTmv, pt);
            pt.close();
        }

        pt = new PrintWriter(nomeFicheiros[2]);
        geraFicheiroArrBidimensional(resultados, pt);
        pt.close();

        pt = new PrintWriter(nomeFicheiros[3]);
        geraFicheiroArrBidimensional(distribuicaoNormalizada, pt);
        pt.close();
        try {
            geraGraficos(especie, resultados.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void geraGraficos(String especie, int numClasses) throws IOException {
        mostraGraficos(especie, numClasses, "png");
        String formato;
        if (modoInterativo) {
            formato = formataFicheiro();
        }else{
            formato=fileType;
        }
        gravaGraficos(especie, numClasses, formato);
    }

    public static String formataFicheiro() {
        int formato;
        String formatoFinal;
        do {
            System.out.print("1-PNG\n2-EPS\n-->");
            formato = sc.nextInt();
        } while ((formato != 1) && (formato != 2));
        switch (formato) {
            case 1:
                formatoFinal = "png";
                break;
            default:
                formatoFinal = "eps";
                break;
        }
        return formatoFinal;

    }

    public static void gravaGraficos(String especie, int numClasses, String extensao) throws IOException {
        String ficheiroInput, tituloDoGrafico, ficheiroOutput;
        for (int i = 0; i < 4; i++) {

            File f = new File(nomeFicheiroGnu[i]);
            try {//se o ficheiro existir, apaga-o
                f.delete();
            } catch (Exception e) {

            }
            FileWriter fw = new FileWriter(nomeFicheiroGnu[i]);
            PrintWriter pt = new PrintWriter(fw);
            ficheiroInput = nomeFicheiros[i];//vão ser os nomes
            LocalDate informacao = LocalDate.now();
            tituloDoGrafico = especie + ", " + titulos[i];//nome do ficheiro
            ficheiroOutput = informacao + "" + especie + "" + tituloDoGrafico;
            pt.println("set terminal " + extensao);
            pt.println("set output '" + ficheiroOutput + "." + extensao + "' ");// nome ficheiro eps ou png

            defineGrafico(pt, tituloDoGrafico, ylabel[i]);
            defineFicheiro(pt, numClasses, tituloSecundario[i], i, ficheiroInput);
            Runtime rt = Runtime.getRuntime();
            try {
                String caminhoCompleto = nomeFicheiroGnu[i];
                Process process = rt.exec("gnuplot " + caminhoCompleto);
            } catch (Exception ex) {
            }
        }
    }

    public static void mostraGraficos(String especie, int numClasses, String extensao) throws IOException {
        String ficheiroInput, tituloDoGrafico;
        for (int i = 0; i < 4; i++) {
            File ficheiro = new File(nomeFicheiroGnu[i]);
            try {//se o ficheiro existir, apaga-o
                ficheiro.delete();
            } catch (Exception e) {

            }
            FileWriter fw = new FileWriter(nomeFicheiroGnu[i]);
            PrintWriter pt = new PrintWriter(fw);
            ficheiroInput = nomeFicheiros[i];//vão ser os nomes
            File ficheiroCextensao = new File(nomeFicheiroGnu[i] + "." + extensao);
            try {//se o ficheiro existir, apaga-o
                ficheiroCextensao.delete();
            } catch (Exception e) {

            }
            tituloDoGrafico = especie + ", " + titulos[i];//nome do ficheiro
            pt.println("set terminal " + extensao); //formato png postscript text
            pt.println("set output '" + tituloDoGrafico + "." + extensao + "' ");// nome ficheiro + eps ou png
            defineGrafico(pt, tituloDoGrafico, ylabel[i]);
            defineFicheiro(pt, numClasses, tituloSecundario[i], i, ficheiroInput);
            Runtime rt = Runtime.getRuntime();
            try {
                String caminhoCompleto = nomeFicheiroGnu[i];
                Process process = rt.exec("gnuplot " + caminhoCompleto);
            } catch (Exception ex) {
            }
        }
    }

    public static void defineFicheiro(PrintWriter pt, int numClasses, String tituloSecundario, int i, String ficheiroInput) {
        String auxiliar = "";
        String auxiliar2 = "";
        String stringFinal = "";
        int i2;
        int[] tipoLiCr = new int[2];//tipoLiCr[0]-> cor, tipoLiCr[1]-> tipo de linha
        for (i2 = 0; i2 < numClasses; i2++) {
            auxiliar = "1:" + (i2 + 2);
            if (i > 1) {
                defineTipos(tipoLiCr, i2);
                auxiliar2 = "using " + auxiliar + " title '" + tituloSecundario + " " + (i2 + 1) + "' with linespoints linetype " + tipoLiCr[1] + " linecolor " + tipoLiCr[0] + ", ";
            } else {
                auxiliar2 = "using " + auxiliar + " title '" + tituloSecundario + "' with linespoints linetype 7 linecolor -1, ";
            }
            stringFinal = stringFinal + "'" + ficheiroInput + "' " + auxiliar2;
        }
        pt.print("plot " + stringFinal);
        pt.close();
    }

    public static void defineTipos(int[] tipoLinhaCor, int numClasses) {
        int[] tipos;
        tipos = new int[]{1, 2, 3, 4, 5, 8, 9, 10, 11, 15, 16, 17, 18, 19, 20};
        tipoLinhaCor[0] = tipos[((numClasses) % 15)];//cor
        tipoLinhaCor[1] = tipos[((numClasses) / 15)];//tipo de linha
    }

    public static void defineGrafico(PrintWriter pt, String tituloDoGrafico, String ylabel) {
        pt.println("set title  '" + tituloDoGrafico + "'");
        pt.println("set xlabel 'Geração");
        pt.println("set ylabel '" + ylabel + "' ");
    }


    public static void geraFicheiro(double[] array, PrintWriter pt) {
        for (int i = 0; i < array.length; i++) {
            pt.printf("%d%5c%f\n", i, c, array[i]);
        }
    }

    public static void geraFicheiroArrBidimensional(double[][] array, PrintWriter pt) {
        int i, i2;
        for (i = 0; i < array.length; i++) {
            pt.printf("%d", i);
            for (i2 = 0; i2 < array[i].length; i2++) {
                pt.printf("%5c%.2f", c, array[i][i2]);
            }
            pt.print("\n");
        }
    }


    public static void registaResultados(double[][] resultados, double[][] matriz, double[][] intervalos) {// a evolução de cada uma das classes//
        double[][] temp;
        for (int i = 0; i < resultados.length; i++) {
            temp = calculaProduto(exponenteMatriz(matriz, i), intervalos);
            auxiliarResultados(resultados[i], temp);
        }
    }

    public static void auxiliarResultados(double[] resultados, double[][] matrizColuna) {
        for (int i = 0; i < resultados.length; i++) {
            resultados[i] = matrizColuna[i][0];
        }
    }

    public static void registaPopulacaoTotal(double[] total, double[][] resultados) {//evolução da população total//
        double acml;
        for (int i = 0; i < total.length; i++) {
            acml = 0;
            for (int i2 = 0; i2 < resultados[i].length; i2++) {
                acml = acml + resultados[i][i2];
            }
            total[i] = acml;
        }
    }

    public static void registaTaxaVariacao(double[] tmv, double[] total) {// taxa de variação do total//
        if (!excecao) {
            for (int i = 0; i < tmv.length; i++) {
                tmv[i] = total[i + 1] - total[i];
            }
        }
    }

    public static void registaDistribuicaoNormalizada(double[] total, double[][] resultados, double[][] distribuicaoNormalizada) {// a percentagem de cada classe//
        for (int i = 0; i < distribuicaoNormalizada.length; i++) {
            for (int i2 = 0; i2 < distribuicaoNormalizada[i].length; i2++) {
                distribuicaoNormalizada[i][i2] = (resultados[i][i2] / total[i]) * 100;
            }
        }
    }


    public static double[][] exponenteMatriz(double[][] matriz, int expoente) {// calcula uma matriz elevada a um dado expoente
        double[][] matrizResultado = matriz;
        double[][] matrizIdentidade = new double[matriz.length][matriz[0].length];
        int i, i2;
        for (i = 0; i < matrizIdentidade.length; i++) {
            for (i2 = 0; i2 < matrizIdentidade[i].length; i2++) {
                if (i == i2) {
                    matrizIdentidade[i][i2] = 1;
                } else {
                    matrizIdentidade[i][i2] = 0;
                }
            }
        }
        if (expoente > 0) {
            for (i = 1; i < expoente; i++) {
                matrizResultado = calculaProduto(matrizResultado, matriz);
            }
            return matrizResultado;
        } else {
            return matrizIdentidade;
        }
    }

    public static void arredondamento(double[] array, double valorArredondamento) {
        for (int i = 0; i < array.length; i++) {
            array[i] = auxiliarArredondamento(array[i], valorArredondamento);
        }
    }

    public static void arredondamentoArrayBidimensional(double[][] array, double valorArredondamento) {
        for (int i = 0; i < array.length; i++) {
            for (int i2 = 0; i2 < array[i].length; i2++) {
                array[i][i2] = auxiliarArredondamento(array[i][i2], valorArredondamento);
            }
        }
    }

    /////////////////////////////////////////////////////JOAO////////////////////////////////////////////////


}   // Class End.
